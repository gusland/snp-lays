import 'normalize.css';
import '../../../fonts/index.css';
import './styles/_about_block.sass';
import './styles/_animate.sass';
import './styles/_body.sass';
import './styles/_container.sass';
import './styles/_footer.sass';
import './styles/_gallery_block.sass';
import './styles/_header.sass';
import './styles/_icons_block.sass';
import './styles/_layz_block.sass';
import './styles/_main.sass';
import './styles/_menu.sass';
import './styles/_slide_block.sass';
import './styles/_utilites.sass';

