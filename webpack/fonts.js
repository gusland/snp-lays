module.exports = function(paths) {
  return {
    module: {
      rules: [
        {
          test: /\.(otf|woff2|eot|ttf|svg)$/,
          loader:  'file-loader',
          options: {
            name: 'fonts/[name].[ext]'
          }
        }
      ]
    }
  };
};